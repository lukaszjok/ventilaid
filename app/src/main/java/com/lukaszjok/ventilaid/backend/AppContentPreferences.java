package com.lukaszjok.ventilaid.backend;

import android.content.Context;
import android.content.SharedPreferences;


public class AppContentPreferences {
    SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public Context context;
    int PRIVATE_MODE = 0;


    public static final String PREF_NAME = "APP_CONTENT_PREFERENCES";
    public static final String APP_CONTACT_MAIL = "APP_CONTACT_MAIL";

    public static final String WEBSERVICE_HOST = "WEBSERVICE_HOST";
    public static final String WEBSERVICE_HTTP_PORT = "WEBSERVICE_HTTP_PORT";
    public static final String WEBSERVICE_HTTPS_PORT = "WEBSERVICE_HTTPS_PORT";


    public AppContentPreferences(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public String getAppContactMail(){
        return sharedPreferences.getString(APP_CONTACT_MAIL,"");
    }
    public void setAppContactMail(String mail){
        editor.putString(APP_CONTACT_MAIL,mail);
        editor.apply();
    }

    public String getWebserviceHost(){
        return sharedPreferences.getString(WEBSERVICE_HOST,"");
    }

    public void setWebserviceHost(String ip){
        editor.putString(WEBSERVICE_HOST,ip);
        editor.apply();
    }

    public int getWebserviceHttpPort(){
        return sharedPreferences.getInt(WEBSERVICE_HTTP_PORT,-1);
    }

    public void setWebserviceHttpPort(int port){
        editor.putInt(WEBSERVICE_HTTP_PORT,port);
        editor.apply();
    }

    public int getWebserviceHttpsPort(){
        return sharedPreferences.getInt(WEBSERVICE_HTTPS_PORT,-1);
    }
    public void setWebserviceHttpsPort(int port){
        editor.putInt(WEBSERVICE_HTTPS_PORT,port);
        editor.apply();
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }




}
