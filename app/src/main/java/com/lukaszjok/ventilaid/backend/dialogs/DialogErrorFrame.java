package com.lukaszjok.ventilaid.backend.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;

import com.lukaszjok.ventilaid.R;


public class DialogErrorFrame extends DialogFragment {
    public interfaceDialogEditMessageChoiceListener dialogEditMessageChoiceListener;
    private String errorCode;


    public DialogErrorFrame() {

    }

    public interface interfaceDialogEditMessageChoiceListener {
        void onDialogErrorFramePositiveListener(DialogFragment dialog);
        void onDialogErrorFrameNegativeListener(DialogFragment dialog);
        void onDialogErrorFrameCancelListener(DialogFragment dialog);
        void onDialogErrorFrameDismissListener(DialogFragment dialog);
    }

    public void setDialogEditMessageChoiceListener(interfaceDialogEditMessageChoiceListener dialogEditMessageChoiceListener){
        this.dialogEditMessageChoiceListener = dialogEditMessageChoiceListener;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getResources().getString(R.string.dialog_title_confirm_edit_message));
        alertDialogBuilder.setMessage(getResources().getString(R.string.dialog_message_confirm_edit_message)+errorCode);
        alertDialogBuilder.setIcon(R.drawable.ic_error_24px);

        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_yes),  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success
                dialogEditMessageChoiceListener.onDialogErrorFramePositiveListener(DialogErrorFrame.this);
            }
        });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogEditMessageChoiceListener.onDialogErrorFrameNegativeListener(DialogErrorFrame.this);

            }

        });


        return alertDialogBuilder.create();

    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        dialogEditMessageChoiceListener.onDialogErrorFrameDismissListener(DialogErrorFrame.this);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);

        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        dialogEditMessageChoiceListener.onDialogErrorFrameCancelListener(DialogErrorFrame.this);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnCancelListener) activity).onCancel(dialog);

        }
    }
}