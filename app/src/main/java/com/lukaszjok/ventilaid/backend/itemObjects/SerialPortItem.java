package com.lukaszjok.ventilaid.backend.itemObjects;

public class SerialPortItem {
    private String port;
    private String desc;
    private String hwid;

    public SerialPortItem(String port, String desc, String hwid){
        this.port = port;
        this.desc = desc;
        this.hwid = hwid;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getHwid() {
        return hwid;
    }

    public void setHwid(String hwid) {
        this.hwid = hwid;
    }
}
