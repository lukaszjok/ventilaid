package com.lukaszjok.ventilaid.backend.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lukaszjok.ventilaid.R;
import com.lukaszjok.ventilaid.backend.itemObjects.AndroidLicenseItem;
import com.lukaszjok.ventilaid.backend.itemObjects.SerialPortItem;

import java.util.ArrayList;


public class AdapterSerialPortsList extends BaseAdapter {
    private Context ctx;
    private ArrayList<SerialPortItem> mData = new ArrayList();
    private LayoutInflater mInflater;

    private class ViewHolder {
        public TextView tvPort;
        public TextView tvDesc;
        public TextView tvHwid;

    }

    public AdapterSerialPortsList(Context ctx) {
        this.ctx = ctx;
    }

    public void addItem(final SerialPortItem item) {
        mData.add(item);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public SerialPortItem getItem(int position) {

        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    public void deleteItems() {
        mData = new ArrayList<SerialPortItem>();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("getView " + position + " " + convertView);
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.serial_port_item, parent, false);

            holder = new ViewHolder();
            holder.tvPort = convertView.findViewById(R.id.tv_serial_port_item_port);
            holder.tvDesc =  convertView.findViewById(R.id.tv_serial_port_item_desc);
            holder.tvHwid =  convertView.findViewById(R.id.tv_serial_port_item_hwid);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvPort.setText(mData.get(position).getPort());
        holder.tvDesc.setText(mData.get(position).getDesc().replaceAll("\\\\n", "\n"));
        holder.tvHwid.setText(mData.get(position).getHwid().replaceAll("\\\\n", "\n"));

        return convertView;
    }

}

