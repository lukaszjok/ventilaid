package com.lukaszjok.ventilaid.backend.itemObjects;

public class AndroidLicenseItem {
    private String libraryName;    private String description;


    public AndroidLicenseItem(){
        this.libraryName = "";
        this.description = "";
    }

    public AndroidLicenseItem(String libraryName, String description){
        this.libraryName = libraryName;
        this.description = description;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
