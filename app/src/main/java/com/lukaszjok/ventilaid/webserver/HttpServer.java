package com.lukaszjok.ventilaid.webserver;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class HttpServer {
    private int port;
    private Handler defaultHandler = null;
    // Two level map: first level is HTTP Method (GET, POST, OPTION, etc.), second level is the
    // request paths.
    private Map<String, Map<String, Handler>> handlers = new HashMap<String, Map<String, Handler>>();

    public interfaceHttpHandlerListener interfaceHttpHandlerListener;

    public interface interfaceHttpHandlerListener{
        void onDataFrameStatusRequestHandler(String hi_press, String lo_press, String breaths,
                                             String breaths_proportion, String air_volume);
        void onDataFrameErrorRequestHandler(String err_code);

    }

    public void setInterfaceHttpHandlerListener(HttpServer.interfaceHttpHandlerListener interfaceHttpHandlerListener) {
        this.interfaceHttpHandlerListener = interfaceHttpHandlerListener;
    }

    // TODO SSL support
    public HttpServer(int port) {
        this.port = port;
    }

    /**
     * @param path if this is the special string "/*", this is the default handler if
     *             no other handler matches.
     */
    public void addHandler(String method, String path, Handler handler) {
        Map<String, Handler> methodHandlers = handlers.get(method);
        if (methodHandlers == null) {
            methodHandlers = new HashMap<String, Handler>();
            handlers.put(method, methodHandlers);
        }
        methodHandlers.put(path, handler);
    }

    public void start() {
        ServerSocket socket = null;
        try {
            socket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Listening on port " + port);
        Socket client = null;
        while (true) {
            try {
                if (!((client = socket.accept()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Received connection from " + client.getRemoteSocketAddress().toString());
            SocketHandler handler = new SocketHandler(client, handlers);
            Thread t = new Thread(handler);
            t.start();
        }
    }

    public void addAllHandlers() {
        getDataFrameStatusHandler();
        getDataFrameErrorHandler();
        fileHandler();
        helloWorldHandler();
    }

    private void getDataFrameStatusHandler() {
        addHandler("GET", "/data_frame", new com.lukaszjok.ventilaid.webserver.Handler() {
            public void handle(Request request, Response response) throws IOException {
                interfaceHttpHandlerListener.
                        onDataFrameStatusRequestHandler
                                (request.getParameter("hi_press"),
                                        request.getParameter("lo_press"),
                                        request.getParameter("breaths"),
                                        request.getParameter("breaths_proportion"),
                                        request.getParameter("air_volume"));

                String html = "hi_press: " + request.getParameter("hi_press") + "<br>";
                html += "lo_press: " + request.getParameter("lo_press") + "";
                html += "breaths: " + request.getParameter("breaths") + "";
                html += "breaths_proportion: " + request.getParameter("breaths_proportion") + "";
                html += "air_volume: " + request.getParameter("air_volume") + "";
                response.setResponseCode(200, "OK");
                response.addHeader("Content-Type", "text/html");
                response.addBody(html);
            }
        });
    }

    private void getDataFrameErrorHandler() {
        addHandler("GET", "/data_frame_error", new com.lukaszjok.ventilaid.webserver.Handler() {
            public void handle(Request request, Response response) throws IOException {
                interfaceHttpHandlerListener.
                        onDataFrameErrorRequestHandler(request.getParameter("err_code"));

                String html = "err_code: " + request.getParameter("err_code") + "<br>";
                response.setResponseCode(200, "OK");
                response.addHeader("Content-Type", "text/html");
                response.addBody(html);
            }
        });
    }


    private void helloWorldHandler() {
        addHandler("GET", "/hello", new com.lukaszjok.ventilaid.webserver.Handler() {
            public void handle(Request request, Response response) throws IOException {
                String html = "Hello World! It works, your name is:" + request.getParameter("name") + "";
                response.setResponseCode(200, "OK");
                response.addHeader("Content-Type", "text/html");
                response.addBody(html);
            }
        });
    }

    private void fileHandler() {
        addHandler("GET", "/*", new FileHandler());  // Default handler
    }


}