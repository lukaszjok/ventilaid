package com.lukaszjok.ventilaid.webserver;

import java.util.Map;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class Request  {
    private String method;
    private String path;
    private String fullUrl;
    private Map<String, String> headers = new HashMap<String, String>();
    private Map<String, String> queryParameters = new HashMap<String, String>();
    private BufferedReader in;

    public Request(BufferedReader in)  {
        this.in = in;
    }

    public String getMethod()  {
        return method;
    }

    public String getPath()  {
        return path;
    }

    public String getFullUrl()  {
        return fullUrl;
    }

    // TODO support mutli-value headers
    public String getHeader(String headerName)  {
        return headers.get(headerName);
    }

    public String getParameter(String paramName)  {
        return queryParameters.get(paramName);
    }

    private void parseQueryParameters(String queryString)  {
        for (String parameter : queryString.split("&"))  {
            int separator = parameter.indexOf('=');
            if (separator > -1)  {
                queryParameters.put(parameter.substring(0, separator),
                        parameter.substring(separator + 1));
            } else  {
                queryParameters.put(parameter, null);
            }
        }
    }

    public boolean parse() throws IOException  {
        String initialLine = in.readLine();
        System.out.println(">>>>>1 "+ initialLine);
        log(initialLine);
        StringTokenizer tok = new StringTokenizer(initialLine);
        System.out.println(">>>>>2 "+ tok);
        String[] components = new String[3];
        for (int i = 0; i < components.length; i++)  {
            // TODO support HTTP/1.0?
            if (tok.hasMoreTokens())  {
                components[i] = tok.nextToken();
                System.out.println(">>>>>[ "+i+"]"+ components[i]);
            } else  {
                return false;
            }
        }

        method = components[0];
        System.out.println(">>>>> method: "+ method);
        fullUrl = components[1];
        System.out.println(">>>>> fullUrl: "+ fullUrl);

        // Consume headers
        while (true)  {
            String headerLine = in.readLine();
            log(headerLine);
            System.out.println(">>>>> headline: "+ headerLine);
            if (headerLine.length() == 0)  {
                break;
            }

            int separator = headerLine.indexOf(":");
            if (separator == -1)  {
                return false;
            }
            headers.put(headerLine.substring(0, separator),
                    headerLine.substring(separator + 1));

        }
        System.out.println(">>>>> headers : "+ headers);

        // TODO should look for host header, Connection: Keep-Alive header,
        // Content-Transfer-Encoding: chunked

        if (components[1].indexOf("?") == -1)  {
            path = components[1];
        } else  {
            path = components[1].substring(0, components[1].indexOf("?"));
            parseQueryParameters(components[1].substring(
                    components[1].indexOf("?") + 1));
        }

        if ("/".equals(path))  {
            path = "/index.html";
        }

        System.out.println(">>>>> path : "+ path);

        return true;
    }

    private void log(String msg)  {
        System.out.println(msg);
    }

    public String toString()  {
        return method  + " " + path + " " + headers.toString();
    }
}