package com.lukaszjok.ventilaid.gui;

import android.Manifest;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.snackbar.Snackbar;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.ventilaid.backend.CustomTools;

import com.google.android.material.navigation.NavigationView;
import com.lukaszjok.ventilaid.R;
import com.lukaszjok.ventilaid.backend.AppContentPreferences;
import com.lukaszjok.ventilaid.backend.CustomXAxisRenderer;
import com.lukaszjok.ventilaid.backend.Icicle;
import com.lukaszjok.ventilaid.backend.dialogs.DialogErrorFrame;
import com.lukaszjok.ventilaid.webserver.HttpServer;
import com.lukaszjok.ventilaid.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnChartValueSelectedListener, ActivityCompat.OnRequestPermissionsResultCallback,
        HttpServer.interfaceHttpHandlerListener, DialogErrorFrame.interfaceDialogEditMessageChoiceListener{

    private AppContentPreferences appContentPreferences;
    private SharedPreferences sharedPreferences;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean isDialogErrorShown;
    private int failedHttpRequestCounter;
    private ProgressBar pbLoading;
    private static String DBG_TAG;

    //hi_press ---
    private ImageButton imgBtnHiPressEnterNewValue;
    private ImageButton imgBtnHiPressResetValue;

    private EditText etHiPress;
    private TextView tvHiPress;
    private SeekBar sbHiPress;
    private Switch swHiPress;
    private float hiPressCurrentVal;
    private int totalMaxValueHiPress;

    //lo_press ---
    private ImageButton imgBtnLoPressEnterNewValue;
    private ImageButton imgBtnLoPressResetValue;
    private EditText etLoPress;
    private TextView tvLoPress;
    private SeekBar sbLoPress;
    private Switch swLoPress;
    private float loPressCurrentVal;
    private int totalMaxValueLoPress;

    //breaths ---
    private ImageButton imgBtnBreathsEnterNewValue;
    private ImageButton imgBtnHBreathResetValue;
    private EditText etBreaths;
    private TextView tvBreaths;
    private SeekBar sbBreaths;
    private Switch swBreaths;
    private float breathsCurrentVal;
    private int totalMaxValueBreaths;

    //breaths_proportion ---
    private ImageButton imgBtnBreathsProportionEnterNewValue;
    private ImageButton imgBtnBreathProportionResetValue;
    private EditText etBreathsProportion;
    private TextView tvBreathsProportion;
    private SeekBar sbBreathsProportion;
    private Switch swBreathsProportion;
    private float breathsProportionCurrentVal;
    private int totalMaxValueBreathsProportion;

    //air_volume ---
    private ImageButton imgBtnAirVolumeEnterNewValue;
    private ImageButton imgBtnAirValueResetValue;
    private EditText etAirVolume;
    private TextView tvAirVolume;
    private SeekBar sbAirVolume;
    private Switch swAirVolume;
    private float airVolumeCurrentVal;
    private int totalMaxValueAirVolume;

    private LineChart chartMain;
    protected Typeface tfRegular;
    protected Typeface tfLight;
    List<Integer> dataLineColors;
    List<String> dataLineLabels;

    private DecimalFormat df;

    private float maxXaxisVal;


    private static final int PERMISSION_STORAGE = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icicle.load(savedInstanceState, this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initControlsAndVariables();
        initListeners();




        float pomiar = 3.14f;                    Log.i(DBG_TAG, String.valueOf(pomiar));


        RESTapi.initAPI(MainActivity.this,
                sharedPreferences.getString(getApplicationContext().getResources().getString(R.string.pref_key_remote_webservice_host),"0.0.0.0"),
                Integer.parseInt(sharedPreferences.getString(getApplicationContext().getResources().getString(R.string.pref_key_remote_webservice_http_port),"8080")),
                Integer.parseInt(sharedPreferences.getString(getApplicationContext().getResources().getString(R.string.pref_key_remote_webservice_https_port),"443"))
        );

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Thread thread = new Thread(new Runnable() {

            @Override
            public synchronized void run() {
                try {


                    HttpServer server = new HttpServer(Integer.parseInt(sharedPreferences.getString(getApplicationContext().getResources().getString(R.string.pref_key_android_webserver_http_port),"8080")));
                    server.setInterfaceHttpHandlerListener(MainActivity.this);
                    server.addAllHandlers();
                    server.start();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }


    private void initControlsAndVariables() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this /* Activity context */);
        appContentPreferences = new AppContentPreferences(this);

        doubleBackToExitPressedOnce = false;
        isDialogErrorShown = false;
        failedHttpRequestCounter = 0;
        pbLoading = findViewById(R.id.progressBar_main_activity);
        pbLoading.setVisibility(View.GONE);
        DBG_TAG = "dbg";
/*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            tfRegular =  getResources().getFont(R.font.opensans_regular);
            tfLight = getResources().getFont(R.font.opensans_light);
        }

 */

        tfRegular =  ResourcesCompat.getFont(getApplicationContext(), R.font.opensans_regular);
        tfLight = ResourcesCompat.getFont(getApplicationContext(), R.font.opensans_light);

        df = new DecimalFormat("#");
        //maxXaxisVal = 60.00f;
        maxXaxisVal = (float)(int)(System.currentTimeMillis()+1000*5);

        // hi_press ---------------
        totalMaxValueHiPress = 100;
        hiPressCurrentVal = 0.00f;
        imgBtnHiPressEnterNewValue = findViewById(R.id.img_btn_hi_press_enter_new_value);
        imgBtnHiPressResetValue = findViewById(R.id.img_btn_hi_press_reset_value);
        swHiPress = findViewById(R.id.sw_hi_press_shown);
        etHiPress = findViewById(R.id.et_hi_press);
        tvHiPress = findViewById(R.id.tv_hi_press);
        sbHiPress = findViewById(R.id.sb_hi_press);
        //sbHiPress.setMin(0);
        sbHiPress.setMax(totalMaxValueHiPress *10);

        // lo_press ---------------
        totalMaxValueLoPress = 100;
        loPressCurrentVal = 0.00f;
        imgBtnLoPressEnterNewValue = findViewById(R.id.img_btn_lo_press_enter_new_value);
        imgBtnLoPressResetValue = findViewById(R.id.img_btn_lo_press_reset_value);
        swLoPress = findViewById(R.id.sw_lo_press_shown);
        etLoPress = findViewById(R.id.et_lo_press);
        tvLoPress = findViewById(R.id.tv_lo_press);
        sbLoPress = findViewById(R.id.sb_lo_press);
        //sbLoPress.setMin(0);
        sbLoPress.setMax(totalMaxValueLoPress *10);

        // breaths ---------------
        totalMaxValueBreaths = 100;
        breathsCurrentVal = 0.00f;
        imgBtnBreathsEnterNewValue = findViewById(R.id.img_btn_breaths_enter_new_value);
        imgBtnHBreathResetValue = findViewById(R.id.img_btn_breaths_reset_value);
        swBreaths = findViewById(R.id.sw_breaths_shown);
        etBreaths = findViewById(R.id.et_breaths);
        tvBreaths = findViewById(R.id.tv_breaths);
        sbBreaths = findViewById(R.id.sb_breaths);
        //sbBreaths.setMin(0);
        sbBreaths.setMax(totalMaxValueBreaths *10);

        // breaths_proportion ---------------
        totalMaxValueBreathsProportion = 100;
        breathsProportionCurrentVal = 0.00f;
        imgBtnBreathsProportionEnterNewValue = findViewById(R.id.img_btn_breaths_proportion_enter_new_value);
        imgBtnBreathProportionResetValue = findViewById(R.id.img_btn_breaths_proportion_reset_value);
        swBreathsProportion = findViewById(R.id.sw_breaths_proportion_shown);
        etBreathsProportion = findViewById(R.id.et_breaths_proportion);
        tvBreathsProportion = findViewById(R.id.tv_breaths_proportion);
        sbBreathsProportion = findViewById(R.id.sb_breaths_proportion);
        //sbBreathsProportion.setMin(0);
        sbBreathsProportion.setMax(totalMaxValueBreathsProportion *10);

        // air_volume ---------------
        totalMaxValueAirVolume = 100;
        airVolumeCurrentVal = 0.00f;
        imgBtnAirVolumeEnterNewValue = findViewById(R.id.img_btn_air_volume_enter_new_value);
        imgBtnAirValueResetValue = findViewById(R.id.img_btn_air_volume_reset_value);
        swAirVolume = findViewById(R.id.sw_air_volume_shown);
        etAirVolume = findViewById(R.id.et_air_volume);
        tvAirVolume = findViewById(R.id.tv_air_volume);
        sbAirVolume = findViewById(R.id.sb_air_volume);
        //sbAirVolume.setMin(0);
        sbAirVolume.setMax(totalMaxValueAirVolume *10);


        //Chart Settings---------------------------
        //Main Chart Settings
        dataLineColors = new ArrayList<>();
        dataLineColors.add(getApplicationContext().getResources().getColor(R.color.correct,getTheme()));
        dataLineColors.add(getApplicationContext().getResources().getColor(R.color.warning,getTheme()));
        dataLineColors.add(getApplicationContext().getResources().getColor(R.color.info,getTheme()));
        dataLineColors.add(getApplicationContext().getResources().getColor(R.color.pink,getTheme()));
        dataLineColors.add(getApplicationContext().getResources().getColor(R.color.orange,getTheme()));

        dataLineLabels = new ArrayList<>();
        dataLineLabels.add("hi_press");
        dataLineLabels.add("lo_press");
        dataLineLabels.add("breaths");
        dataLineLabels.add("bre_pro");
        dataLineLabels.add("air_value");
        chartMain = findViewById(R.id.chart_main);
        chartMain.setOnChartValueSelectedListener(this);

        // enable description text
        chartMain.getDescription().setEnabled(true);

        // enable touch gestures
        chartMain.setTouchEnabled(true);

        // enable scaling and dragging
        chartMain.setDragEnabled(true);
        chartMain.setScaleEnabled(true);
        chartMain.setDrawGridBackground(false);



        //XAxisRenderer - make more - multiline in xAxis label (value)
        //hartMain.setXAxisRenderer(new CustomXAxisRenderer(chartMain.getViewPortHandler(), chartMain.getXAxis(), chartMain.getTransformer(YAxis.AxisDependency.LEFT)));

        // if disabled, scaling can be done on x- and y-axis separately
        chartMain.setPinchZoom(true);

        // set an alternative background color
        chartMain.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.jet,getTheme()));

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        chartMain.setData(data);

        // get the legend (only possible after setting data)
        Legend l = chartMain.getLegend();

        // modify the legend ...
        l.setForm(LegendForm.LINE);
        l.setTypeface(tfLight);
        l.setTextColor(Color.WHITE);

        XAxis xl = chartMain.getXAxis();
        xl.setTypeface(tfLight);
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        xl.setAxisMinimum((int)(System.currentTimeMillis()));
        xl.setAxisMaximum(maxXaxisVal);
        xl.setEnabled(false);
        //xl.setYOffset(40);

        xl.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                SimpleDateFormat sdf = new SimpleDateFormat(":ss");
                String newFormatter = sdf.format(new Date((long)value));
                return newFormatter;
            }
        });





        YAxis leftAxis = chartMain.getAxisLeft();
        leftAxis.setTypeface(tfLight);
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setAxisMaximum(100f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = chartMain.getAxisRight();
        rightAxis.setEnabled(false);

        //END Main Chart Settings


    }

    private void initListeners() {

        // hi_press ---
        imgBtnHiPressEnterNewValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //sendFrame();
            }
        });

        imgBtnHiPressResetValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(DBG_TAG,"reset click!");
            }
        });

        etHiPress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etHiPress.getText().toString().length()>0){
                    hiPressCurrentVal = Float.parseFloat(etHiPress.getText().toString().replace(",","."));
                    //for prevent input over max value
                    if(hiPressCurrentVal > (float) totalMaxValueHiPress){
                        hiPressCurrentVal = (float) totalMaxValueHiPress;
                        etHiPress.setText(df.format(hiPressCurrentVal).replace(",","."));
                        Toast.makeText(getApplicationContext(),"Be careful! It is over the range!",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvHiPress.setText(df.format(hiPressCurrentVal).replace(",","."));
                sbHiPress.setProgress((int) (hiPressCurrentVal *10));
            }
        });

        sbHiPress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                hiPressCurrentVal = CustomTools.getConvertedValue(progress,10.00f);
                tvHiPress.setText(df.format(hiPressCurrentVal).replace(",","."));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                etHiPress.setText(df.format(hiPressCurrentVal).replace(",","."));
            }
        });

        swHiPress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Log.i(DBG_TAG,"CHECKED!");

                }else{
                    Log.i(DBG_TAG,"NOT CHECKED!");
                }
            }
        });

        // lo_press ---
        imgBtnLoPressEnterNewValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sendFrame(loPressCurrentVal);
            }
        });

        imgBtnHiPressResetValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(DBG_TAG,"reset click!");
            }
        });

        etLoPress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etLoPress.getText().toString().length()>0){
                    loPressCurrentVal = Float.parseFloat(etLoPress.getText().toString().replace(",","."));
                    //for prevent input over max value
                    if(loPressCurrentVal > (float) totalMaxValueLoPress){
                        loPressCurrentVal = (float) totalMaxValueLoPress;
                        etLoPress.setText(df.format(loPressCurrentVal).replace(",","."));
                        Toast.makeText(getApplicationContext(),"Be careful! It is over the range!",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvLoPress.setText(df.format(loPressCurrentVal).replace(",","."));
                sbLoPress.setProgress((int) (loPressCurrentVal *10));
            }
        });

        sbLoPress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                loPressCurrentVal = CustomTools.getConvertedValue(progress,10.00f);
                tvLoPress.setText(df.format(loPressCurrentVal).replace(",","."));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                etLoPress.setText(df.format(loPressCurrentVal).replace(",","."));
            }
        });

        swLoPress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Log.i(DBG_TAG,"CHECKED!");

                }else{
                    Log.i(DBG_TAG,"NOT CHECKED!");
                }
            }
        });

        // breaths ---
        imgBtnBreathsEnterNewValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFrame("0","0",String.valueOf((int)breathsCurrentVal),String.valueOf((int)breathsProportionCurrentVal),
                        "0","0","0","0","0","0","0","0", "0");
            }
        });

        imgBtnHiPressResetValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(DBG_TAG,"reset click!");
            }
        });

        etBreaths.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etBreaths.getText().toString().length()>0){
                    breathsCurrentVal = Float.parseFloat(etBreaths.getText().toString().replace(",","."));
                    //for prevent input over max value
                    if(breathsCurrentVal > (float) totalMaxValueBreaths){
                        breathsCurrentVal = (float) totalMaxValueBreaths;
                        etBreaths.setText(df.format(breathsCurrentVal).replace(",","."));
                        Toast.makeText(getApplicationContext(),"Be careful! It is over the range!",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvBreaths.setText(df.format(breathsCurrentVal).replace(",","."));
                sbBreaths.setProgress((int) (breathsCurrentVal *10));
            }
        });

        sbBreaths.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                breathsCurrentVal = CustomTools.getConvertedValue(progress,10.00f);
                tvBreaths.setText(df.format(breathsCurrentVal).replace(",","."));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                etBreaths.setText(df.format(breathsCurrentVal).replace(",","."));
            }
        });

        swBreaths.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Log.i(DBG_TAG,"CHECKED!");

                }else{
                    Log.i(DBG_TAG,"NOT CHECKED!");
                }
            }
        });

        // breaths_proportion ---
        imgBtnBreathsProportionEnterNewValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFrame("0","0",String.valueOf((int)breathsCurrentVal), String.valueOf((int)breathsProportionCurrentVal),
                        "0","0","0","0","0","0","0","0", "0");

            }
        });

        imgBtnHiPressResetValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(DBG_TAG,"reset click!");
            }
        });

        etBreathsProportion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etBreathsProportion.getText().toString().length()>0){
                    breathsProportionCurrentVal = Float.parseFloat(etBreathsProportion.getText().toString().replace(",","."));
                    //for prevent input over max value
                    if(breathsProportionCurrentVal > (float) totalMaxValueBreathsProportion){
                        breathsProportionCurrentVal = (float) totalMaxValueBreathsProportion;
                        etBreathsProportion.setText(df.format(breathsProportionCurrentVal).replace(",","."));
                        Toast.makeText(getApplicationContext(),"Be careful! It is over the range!",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvBreathsProportion.setText(df.format(breathsProportionCurrentVal).replace(",","."));
                sbBreathsProportion.setProgress((int) (breathsProportionCurrentVal *10));
            }
        });

        sbBreathsProportion.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                breathsProportionCurrentVal = CustomTools.getConvertedValue(progress,10.00f);
                tvBreathsProportion.setText(df.format(breathsProportionCurrentVal).replace(",","."));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                etBreathsProportion.setText(df.format(breathsProportionCurrentVal).replace(",","."));
            }
        });

        swBreathsProportion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Log.i(DBG_TAG,"CHECKED!");

                }else{
                    Log.i(DBG_TAG,"NOT CHECKED!");
                }
            }
        });

        // air_volume ---
        imgBtnAirVolumeEnterNewValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFrame("0","0","0", "0",
                        "0","0","0","0","0","0","0","0", "0");
            }
        });

        imgBtnHiPressResetValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(DBG_TAG,"reset click!");
            }
        });
        etAirVolume.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etAirVolume.getText().toString().length()>0){
                    airVolumeCurrentVal = Float.parseFloat(etAirVolume.getText().toString().replace(",","."));
                    //for prevent input over max value
                    if(airVolumeCurrentVal > (float) totalMaxValueAirVolume){
                        airVolumeCurrentVal = (float) totalMaxValueAirVolume;
                        etAirVolume.setText(df.format(airVolumeCurrentVal).replace(",","."));
                        Toast.makeText(getApplicationContext(),"Be careful! It is over the range!",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvAirVolume.setText(df.format(airVolumeCurrentVal).replace(",","."));
                sbAirVolume.setProgress((int) (airVolumeCurrentVal *10));
            }
        });

        sbAirVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                airVolumeCurrentVal = CustomTools.getConvertedValue(progress,10.00f);
                tvAirVolume.setText(df.format(airVolumeCurrentVal).replace(",","."));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                etAirVolume.setText(df.format(airVolumeCurrentVal).replace(",","."));
            }
        });

        swAirVolume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Log.i(DBG_TAG,"CHECKED!");

                }else{
                    Log.i(DBG_TAG,"NOT CHECKED!");
                }
            }
        });



    }



    @Override
    public void onBackPressed() {
        //--- START: Double click (till 2 seconds) to exit application
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.toast_message_double_click_to_close_app, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
        //--- END: Double click (till 2 seconds) to exit application.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //todo: server.stop()
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            Log.i(DBG_TAG, "go to: about");
            Intent intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_android_licenses) {
            Log.i(DBG_TAG, "go to: android_licenses");
            Intent intent = new Intent(MainActivity.this, AndroidLicensesActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_contact) {
            sendEmail();
        } else if (id == R.id.nav_settings) {
            Log.i(DBG_TAG, "go to: android_settings");
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_serial_port_settings) {
            Log.i(DBG_TAG, "go to: android_serial_port_settings");
            Intent intent = new Intent(MainActivity.this, SerialPortsSettigsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected void sendEmail() {
        Log.i(getString(R.string.sen_email_intent_header), "");

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", appContentPreferences.getAppContactMail(), null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.send_email_subject));
        emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.send_email_message_content_hint));

        try {
            startActivity(Intent.createChooser(emailIntent, getString(R.string.sen_email_intent_header)));
            //finish();
            Log.i("dbg", "Finished sending email...");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, R.string.send_email_no_email_client_installed, Toast.LENGTH_SHORT).show();
        }
    }




    public static Map<String, List<String>> getHttpParameters(String url) {
        final Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
        String parametersStr = url.substring(url.indexOf("?") + 1);
        final String[] pairs = parametersStr.split("&");

        int idx = -1;
        String key = "";
        String value = "";


        for (String pair : pairs) {
            idx = pair.indexOf("=");
            try {
                key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (!query_pairs.containsKey(key)) {
                query_pairs.put(key, new LinkedList<String>());
            }

            try {
                value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            query_pairs.get(key).add(value);
        }
        return query_pairs;
    }

    private void addEntry(float entry) {
        LineData data = chartMain.getData();

        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet(getApplicationContext().getResources().getColor(R.color.correct,getTheme()),"hi_press");
                data.addDataSet(set);
            }
            //Log.i("DBG", "dataset couner: " + set.getEntryCount());

            if(set.getEntryCount() >= maxXaxisVal){
                chartMain.getXAxis().setAxisMaximum(set.getEntryCount());

            }


            data.addEntry(new Entry(set.getEntryCount(), entry), 0);
            data.notifyDataChanged();

            // let the chart know it's data has changed
            chartMain.notifyDataSetChanged();

            // limit the number of visible entries
            chartMain.setVisibleXRangeMaximum(maxXaxisVal);
            //chart.setVisibleYRange(30,100, AxisDependency.LEFT);

            // move to the latest entry
            chartMain.moveViewToX(data.getEntryCount());

            // this automatically refreshes the chart (calls invalidate())
            //chart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);

        }
    }

    private void addEntryMultipleLine(float... entries) {
        LineData data = chartMain.getData();
        //int currentTime =  (int)(System.currentTimeMillis() - 1957100000)/100;
        int currentTime =  (int)(System.currentTimeMillis());
        Log.i(DBG_TAG, "cuttentTime: " + currentTime);
        Log.i(DBG_TAG, "cuttentTime LONG: " + System.currentTimeMillis());
        Log.i(DBG_TAG, "cuttentTime LONG to FLOAT: " + (float)System.currentTimeMillis());


        int numOfEntries = entries.length;
        List<ILineDataSet> sets = new ArrayList<>();


        if (data != null) {
            for(int i = 0; i<numOfEntries; i++){
                sets.add(data.getDataSetByIndex(i));
            }
            for(int j = 0; j<numOfEntries; j++){
                if (sets.get(j) == null) {
                    sets.set(j,createSet(dataLineColors.get(j),dataLineLabels.get(j)));
                    data.addDataSet(sets.get(j));
                }
            }
            for(int k = 0; k<numOfEntries; k++){

                    chartMain.getXAxis().setAxisMaximum((float)currentTime);

            }
            for(int m = 0; m<numOfEntries; m++){
                //data.addEntry(new Entry(sets.get(m).getEntryCount(), entries[m]), m);
                data.addEntry(new Entry(currentTime, entries[m]), m);
                data.notifyDataChanged();

            }


            // let the chart know it's data has changed
            chartMain.notifyDataSetChanged();



            // limit the number of visible entries
            chartMain.setVisibleXRangeMaximum((1000*5));
            //chart.setVisibleYRange(30,100, AxisDependency.LEFT);

            // move to the latest entry
            chartMain.moveViewToX((float)(currentTime));

            // this automatically refreshes the chart (calls invalidate())
            //chart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);

        }
    }

    private LineDataSet createSet(int colorLine, String label) {

        LineDataSet set = new LineDataSet(null, label);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(colorLine);
        //set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setDrawCircles(false);
        //set.setCircleRadius(4f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        return set;
    }

    private Thread thread;

    private void feedMultiple() {

        if (thread != null)
            thread.interrupt();

        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                addEntry((float) (Math.random() * 40) + 30f);
            }
        };

        thread = new Thread(new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < 3000000; i++) {

                    // Don't generate garbage runnables inside the loop.
                    runOnUiThread(runnable);

                    try {
                        Thread.sleep(0);//todo: as parameter
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.realtime, menu);
        return true;
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (thread != null) {
            thread.interrupt();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //saveToGallery(); //todo:
            } else {
                Toast.makeText(getApplicationContext(), "Saving FAILED!", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    protected void requestStoragePermission(View view) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(view, "Write permission is required to save image to gallery", Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_STORAGE);
                        }
                    }).show();
        } else {
            Toast.makeText(getApplicationContext(), "Permission Required!", Toast.LENGTH_SHORT)
                    .show();
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_STORAGE);
        }
    }

    @Override
    public void onDataFrameStatusRequestHandler(String hi_press, String lo_press, String breaths,
                                                String breaths_proportion, String air_volume) {
        String buffHiPress = "0.0";
        String buffLoPress = "0.0";
        String buffBreaths = "0.0";
        String buffBreathsProportion = "0.0";
        String buffAirVolume = "0.0";


        if(swHiPress.isChecked()){
            buffHiPress = hi_press;
        }
        if(swLoPress.isChecked()){
            buffLoPress = lo_press;
        }
        if(swBreaths.isChecked()){
            buffBreaths = breaths;
        }
        if(swBreathsProportion.isChecked()){
            buffBreathsProportion = breaths_proportion;
        }
        if(swAirVolume.isChecked()){
            buffAirVolume = air_volume;
        }

        addEntryMultipleLine(Float.parseFloat(buffHiPress),
                Float.parseFloat(buffLoPress),
                Float.parseFloat(buffBreaths),
                Float.parseFloat(buffBreathsProportion),
                Float.parseFloat(buffAirVolume)
        );
    }

    @Override
    public void onDataFrameErrorRequestHandler(String err_code) {
        if(!isDialogErrorShown){
            isDialogErrorShown = true;
            DialogErrorFrame dialog = new DialogErrorFrame();
            dialog.setErrorCode(err_code);
            dialog.setDialogEditMessageChoiceListener(this);
            dialog.show(getFragmentManager(), "DialogEditMessage");
            ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
            toneG.startTone(ToneGenerator.TONE_SUP_ERROR, 10 * 1000);
        }



    }

    @Override
    public void onDialogErrorFramePositiveListener(DialogFragment dialog) {
    Log.i(DBG_TAG, "dialog yes");
    isDialogErrorShown = false;
    }

    @Override
    public void onDialogErrorFrameNegativeListener(DialogFragment dialog) {
        Log.i(DBG_TAG, "dialog no");
        isDialogErrorShown = false;
    }

    @Override
    public void onDialogErrorFrameCancelListener(DialogFragment dialog) {
        Log.i(DBG_TAG, "dialog cancel");
        isDialogErrorShown = false;
    }

    @Override
    public void onDialogErrorFrameDismissListener(DialogFragment dialog) {
        Log.i(DBG_TAG, "dialog dismiss");
        isDialogErrorShown = false;
    }

    public void sendFrame(String hi_press, String lo_press, String breaths, String breath_pro, String air_vol, String
                          ref_rate, String f0, String f1, String f2, String f3, String f4, String f5, String f6) {
        RequestParams params = new RequestParams();
        params.put("hi_press",hi_press);
        params.put("lo_press",lo_press);
        params.put("breaths",breaths);
        params.put("breath_pro",breath_pro);
        params.put("air_vol",air_vol);
        params.put("ref_rate",ref_rate);
        params.put("f0",f0);
        params.put("f1",f1);
        params.put("f2",f2);
        params.put("f3",f3);
        params.put("f4",f4);
        params.put("f5",f5);
        params.put("f6",f6);
        
        RESTapi.post("/send_frame", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    if(code.toLowerCase().equals("1") && status.toLowerCase().equals("ok")){
                        Toast.makeText(getApplicationContext(), R.string.toast_enter_setting_ok,Toast.LENGTH_SHORT).show();
                        pbLoading.setVisibility(View.GONE);
                    }else if(code.toLowerCase().equals("-1") && status.toLowerCase().equals("not_ok")){
                        Toast.makeText(getApplicationContext(), R.string.toast_enter_setting_not_ok,Toast.LENGTH_SHORT).show();
                        pbLoading.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, send_frame)");
                }
            }



            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure send_frame JSON errorResponse " + throwable.getMessage());
                failedHttpRequestCounter++;

                Toast.makeText(getApplicationContext(), getString(R.string.toast_enter_setting_problem_webservice_connection)+ throwable.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }





    /*
    protected void saveToGallery(Chart chart, String name) {
        if (chart.saveToGallery(name + "_" + System.currentTimeMillis(), 70))
            Toast.makeText(getApplicationContext(), "Saving SUCCESSFUL!",
                    Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getApplicationContext(), "Saving FAILED!", Toast.LENGTH_SHORT)
                    .show();
    }

     */



}
