package com.lukaszjok.ventilaid.gui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.ventilaid.R;
import com.lukaszjok.ventilaid.backend.AppContentPreferences;
import com.lukaszjok.ventilaid.backend.adapters.AdapterSerialPortsList;
import com.lukaszjok.ventilaid.backend.itemObjects.AndroidLicenseItem;
import com.lukaszjok.ventilaid.backend.itemObjects.SerialPortItem;
import com.lukaszjok.ventilaid.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class SerialPortsSettigsActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdapterSerialPortsList adapterSerialPortsList;
    private ListView lvSerialPorts;
    private ArrayList<SerialPortItem> allSerialPortItems;
    private ProgressBar pbLoading;

    private EditText etPort;
    private EditText etBaudrade;
    private EditText etParity;
    private EditText etStopbits;
    private EditText etBytesize;

    private Button btnSumbit;

    private static int failedHttpRequestCounter;
    private static String DBG_TAG;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serial_ports_settings);

        initControlsAndVariables();
        initListeners();

        //--- Set Adapter + loading data ---
        fillSerialPorts(getApplicationContext());
        lvSerialPorts.setAdapter(adapterSerialPortsList);
        initListeners();
        //--- END Set Adapter + loading data ---

        getCurrentSerialSettings(getApplicationContext());

    }

    private void initControlsAndVariables() {
        appContentPreferences = new AppContentPreferences(this);

        lvSerialPorts = findViewById(R.id.lv_serial_port_settings);
        allSerialPortItems = new ArrayList<>();

        pbLoading = findViewById(R.id.progressBar_serial_port_settings_activity);
        pbLoading.setVisibility(View.VISIBLE);
        pbLoading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        etPort = findViewById(R.id.et_serial_port_settings_port);
        etBaudrade = findViewById(R.id.et_serial_port_settings_baudrate);
        etParity= findViewById(R.id.et_serial_port_settings_parity);
        etStopbits = findViewById(R.id.et_serial_port_settings_stopbits);
        etBytesize = findViewById(R.id.et_serial_port_settings_bytesize);

        btnSumbit = findViewById(R.id.btn_serial_port_settings_submit);

        failedHttpRequestCounter = 0;
        DBG_TAG = "dbg";

    }

    private void initListeners() {
        btnSumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pbLoading.setVisibility(View.VISIBLE);
                setSerialPortSettings(
                        etPort.getText().toString(),
                        etBaudrade.getText().toString().toUpperCase(),
                        etParity.getText().toString().toUpperCase(),
                        etStopbits.getText().toString().toUpperCase(),
                        etBytesize.getText().toString().toUpperCase()
                );
            }
        });

    }

    public void fillSerialPorts(Context ctx) {
        adapterSerialPortsList = new AdapterSerialPortsList(this);
        RequestParams params = new RequestParams();

        RESTapi.get("/get_serial_ports_list", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("available_ports");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_available_ports: " + status);
                    Log.i(DBG_TAG,"code_available_ports: " + code);
                    Log.i(DBG_TAG,"desc_aavailable_ports: " + desc);


                    for(int i=0; i<jsonArray.length();i++){
                        allSerialPortItems.add(new SerialPortItem(jsonArray.getJSONObject(i).getString("port"),
                                jsonArray.getJSONObject(i).getString("desc"),
                                jsonArray.getJSONObject(i).getString("hwid")
                        ));
                    }

                    for (int i = 0; i < allSerialPortItems.size(); i++) {
                        adapterSerialPortsList.addItem(allSerialPortItems.get(i));
                    }
                    pbLoading.setVisibility(View.GONE);
                    lvSerialPorts.setAdapter(adapterSerialPortsList);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, get_serial_ports_list");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure get_serial_ports_list city JSON errorResponse " + throwable.getMessage());
                failedHttpRequestCounter++;

                if(failedHttpRequestCounter <3){
                    fillSerialPorts(SerialPortsSettigsActivity.this);
                }else{
                    Toast.makeText(SerialPortsSettigsActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestCounter =0;
                }
            }

        });

    }
    public void setSerialPortSettings( String port, String baudrate, String parity, String stopbits, String bytesize) {
        RequestParams params = new RequestParams();
        params.put("port",port);
        params.put("baudrate",baudrate);
        params.put("parity",parity);
        params.put("stopbits",stopbits);
        params.put("bytesize",bytesize);


        RESTapi.post("/set_serial_port_settings", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    if(code.toLowerCase().equals("1") && status.toLowerCase().equals("ok")){
                        Toast.makeText(getApplicationContext(), R.string.set_serial_settings_ok,Toast.LENGTH_SHORT).show();
                        pbLoading.setVisibility(View.GONE);
                        getCurrentSerialSettings(getApplicationContext());
                    }else{
                        Toast.makeText(getApplicationContext(), R.string.set_serial_settings_not_ok,Toast.LENGTH_SHORT).show();
                        pbLoading.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, enter_setting)");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure enter_setting JSON errorResponse " + throwable.getMessage());
                failedHttpRequestCounter++;

                Toast.makeText(getApplicationContext(), getString(R.string.toast_enter_setting_problem_webservice_connection)+ throwable.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getCurrentSerialSettings(Context ctx) {
        RequestParams params = new RequestParams();

        RESTapi.get("/get_serial_current_settings", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                JSONObject jResult = null;
                JSONArray jsonArray = null;
                try {
                    jResult = (JSONObject) response.get("result");
                    jsonArray = response.getJSONArray("current_settings");

                    String code =  jResult.getString("code");
                    String status =  jResult.getString("status");
                    String desc =  jResult.getString("description");

                    Log.i(DBG_TAG,"status_serial_current_settings: " + status);
                    Log.i(DBG_TAG,"code_serial_current_settings: " + code);
                    Log.i(DBG_TAG,"desc_serial_current_settings: " + desc);

                    etPort.setText(jsonArray.getJSONObject(0).getString("port"));
                    etBaudrade.setText(jsonArray.getJSONObject(0).getString("baudrate"));
                    etParity.setText(jsonArray.getJSONObject(0).getString("parity"));
                    etStopbits.setText(jsonArray.getJSONObject(0).getString("stopbits"));
                    etBytesize.setText(jsonArray.getJSONObject(0).getString("bytesize"));


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(DBG_TAG,"error WebService, serial_current_settings");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.i(DBG_TAG,"failure serial_current_settings city JSON errorResponse " + throwable.getMessage());
                failedHttpRequestCounter++;

                if(failedHttpRequestCounter <3){
                    fillSerialPorts(SerialPortsSettigsActivity.this);
                }else{
                    Toast.makeText(SerialPortsSettigsActivity.this, throwable.getMessage(),Toast.LENGTH_LONG).show();
                    failedHttpRequestCounter =0;
                }
            }

        });

    }
}