package com.lukaszjok.ventilaid.gui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.lukaszjok.ventilaid.R;
import com.lukaszjok.ventilaid.backend.AppContentPreferences;
import com.lukaszjok.ventilaid.backend.adapters.AdapterAndroidLicensesList;
import com.lukaszjok.ventilaid.backend.itemObjects.AndroidLicenseItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AndroidLicensesActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private AdapterAndroidLicensesList androidLicensesListAdapter;
    private ListView lvAndroidLicensesList;
    private ArrayList<AndroidLicenseItem> allAndroidLicensItems;
    private ProgressBar pbLoading;

    private static int failedHttpRequestCounter;
    private static String DBG_TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_licenses);

        initControlsAndVariables();
        initListeners();

        //--- Set Adapter + loading data ---
        fillAndroidLicenses(getApplicationContext());
        lvAndroidLicensesList.setAdapter(androidLicensesListAdapter);
        initListeners();
        //--- END Set Adapter + loading data ---

    }

    private void initControlsAndVariables() {
        appContentPreferences = new AppContentPreferences(this);

        lvAndroidLicensesList = findViewById(R.id.lv_android_licenses);
        allAndroidLicensItems = new ArrayList<>();

        pbLoading = findViewById(R.id.progressBar_android_licenses_activity);
        pbLoading.setVisibility(View.VISIBLE);
        pbLoading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        failedHttpRequestCounter = 0;
        DBG_TAG = "dbg";

    }

    private void initListeners() {

    }

    public void fillAndroidLicenses(Context ctx) {
        androidLicensesListAdapter = new AdapterAndroidLicensesList(this);

        List<List<String>> licenses = new ArrayList<List<String>>();
        ArrayList loopj = new ArrayList(Arrays.asList(getString(R.string.license_name_loopj), getString(R.string.license_name_loopj_desc)));
        ArrayList materialDesign = new ArrayList(Arrays.asList(getString(R.string.license_name_material_design), getString(R.string.license_name_material_design_desc)));
        ArrayList mPAndroidChart = new ArrayList(Arrays.asList(getString(R.string.license_name_mp_android_chart), getString(R.string.license_name_mp_android_chart_desc)));
        licenses.add(loopj);
        licenses.add(materialDesign);
        licenses.add(mPAndroidChart);

        for (int i = 0; i < licenses.size(); i++) {
            allAndroidLicensItems.add(new AndroidLicenseItem(licenses.get(i).get(0), licenses.get(i).get(1)));
        }

        for (int i = 0; i < allAndroidLicensItems.size(); i++) {
            androidLicensesListAdapter.addItem(allAndroidLicensItems.get(i));
        }
        pbLoading.setVisibility(View.GONE);
        lvAndroidLicensesList.setAdapter(androidLicensesListAdapter);
    }
}