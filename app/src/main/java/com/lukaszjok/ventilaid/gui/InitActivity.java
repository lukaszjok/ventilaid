package com.lukaszjok.ventilaid.gui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.lukaszjok.ventilaid.R;
import com.lukaszjok.ventilaid.backend.AppContentPreferences;
import com.lukaszjok.ventilaid.backend.NetworkChangeReceiver;


public class InitActivity extends AppCompatActivity {
    private static String DBG_TAG;
    private ProgressBar pbInitial;
    private TextView tvInitMessage;
    private BroadcastReceiver mNetworkReceiver;
    private AppContentPreferences appContentPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        initControlsAndVariables();

        if (isNetworkAvailable()) {
            //hardcoded delay
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(InitActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 1 * 1000);

        } else {
            pbInitial.setVisibility(View.INVISIBLE);
            tvInitMessage.setText(R.string.init_no_network_message);
        }
    }

    private void initControlsAndVariables() {
        appContentPreferences = new AppContentPreferences(this);
        pbInitial = findViewById(R.id.progressBar_init_activity);
        pbInitial.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
        tvInitMessage = findViewById(R.id.tv_init_message);

        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();

        DBG_TAG = "dbg";
    }

    private boolean isNetworkAvailable() {
        boolean result;
        final ConnectivityManager connMgr = (ConnectivityManager)
                this.getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            //Toast.makeText(this, "Wifi", Toast.LENGTH_LONG).show();
            result = true;
        } else if (mobile.isConnectedOrConnecting()) {
            //Toast.makeText(this, "Mobile 3G ", Toast.LENGTH_LONG).show();
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

}


