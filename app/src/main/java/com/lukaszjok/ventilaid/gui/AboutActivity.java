package com.lukaszjok.ventilaid.gui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lukaszjok.ventilaid.R;
import com.lukaszjok.ventilaid.backend.AppContentPreferences;
import com.lukaszjok.ventilaid.webservice.RESTapi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class AboutActivity extends AppCompatActivity {
    private AppContentPreferences appContentPreferences;
    private static String DBG_TAG;
    private ProgressBar pbLoading;
    private TextView tvAboutContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        initControlsAndVariables();
        initListeners();

        fillAboutContentData(AboutActivity.this);
    }

    private void initControlsAndVariables(){
        appContentPreferences = new AppContentPreferences(this);
        tvAboutContent = findViewById(R.id.tv_about_content);
        pbLoading = findViewById(R.id.progressBar_about_activity);
        pbLoading.setVisibility(View.VISIBLE);
        pbLoading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        DBG_TAG = "dbg";
    }

    private void initListeners(){

    }

    public void fillAboutContentData(Context ctx) {
        String aboutContent="";
        aboutContent = getString(R.string.about_application_info);

        tvAboutContent.setText(aboutContent.replace("\\n","\n").replace("\\t","\t"));
        pbLoading.setVisibility(View.GONE);
    }
}
